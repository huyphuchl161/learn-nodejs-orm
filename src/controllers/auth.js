const services = require("../services");
const {
  internalServerError,
  badRequest,
} = require("../middlewares/handle_errors");
const { email, password } = require("../helpers/joi_schema");
const joi = require("joi");

const register = async (req, res) => {
  try {
    const { error } = joi.object({ email, password }).validate(req.body);
    console.log(error);
    if (error) return badRequest(error.details[0]?.message, res);
    const response = await services.register(req.body);
    return res.status(200).json(response);
  } catch (error) {
    return internalServerError(res);
  }
};

const login = async (req, res) => {
  try {
    const { error } = joi.object({ email, password }).validate(req.body);
    // console.log(error);
    if (error) return badRequest(error.details[0]?.message, res);
    const response = await services.login(req.body);
    return res.status(200).json(response);
  } catch (error) {
    return internalServerError(res);
  }
};

export { register, login };
