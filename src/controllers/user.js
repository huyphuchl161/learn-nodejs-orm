const services = require('../services')
const { internalServerError, badRequest } = require('../middlewares/handle_errors')

const getCurrent = async (req, res) => {
   try{
      const { id } = req.user
      const user = await services.getOne(id)
      console.log(user)
      return res.status(200).json(user)
   } catch(err){
      return internalServerError(res)
      // console.log(err)
   }
}

export {getCurrent}