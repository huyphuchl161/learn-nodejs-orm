const book = require('../services/book')
const {internalServerError, badRequest} = require('../middlewares/handle_errors')
const joi = require('joi')
const { title, price, available, category_code, image } = require('../helpers/joi_schema')

// CREATE
const createNewBook = async (req, res) => {
   console.log(req.body)
   try {
      const { error } = joi.object({title, price, available, category_code, image}).validate(req.body)
      if (error) return badRequest(error.details[0].message, res)
      const response = await book.createNewBook(req.body)
      return res.status(200).json(response)
   } catch (error) {
      return internalServerError(res)
   }
}

module.exports = {createNewBook}