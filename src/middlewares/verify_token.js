const { badRequest, notAuth } = require('./handle_errors')

const jwt = require('jsonwebtoken')

const verifyToken = (req, res, next) => {
   const token = req.headers.authorization
   // console.log(token)
   if (!token) return notAuth('Require authorization', res)
   const accessToken = token.split(' ')[1]
   console.log(accessToken)
   jwt.verify(accessToken, process.env.JWT_SECRET, (err, user) => {
      if(err) return notAuth('Access token may be expired or invalid', res)

      req.user = user
      next()
   })
}

module.exports = verifyToken