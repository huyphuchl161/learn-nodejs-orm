const { notAuth } = require('./handle_errors')

const isAdmin = (req, res, next) => {
   const { role_code } = req.user
   if(role_code !== 'R1') return notAuth('Require role Admin', res)
   next()
}

const isModeratorOrAdmin = (req, res, next) => {
   const { role_code } = req.user
   if(role_code !== 'R1' && role_code !== 'R2') return notAuth('Require role Admin oe Moderator', res)
   next()
}

export {isAdmin, isModeratorOrAdmin}