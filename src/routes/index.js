const user = require('./user')
const auth = require('./auth')
const book = require('./book')
const {notFound} = require('../middlewares/handle_errors')

const initRoutes = (app) => {
   app.use('/api/v1/users', user)
   app.use('/api/v1/auth', auth)
   app.use('/api/v1/book', book)

   app.use(notFound)
}

module.exports = initRoutes