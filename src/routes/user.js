const user = require('../controllers/user')
const verifyToken  = require('../middlewares/verify_token')
const {isAdmin, isModeratorOrAdmin} = require('../middlewares/verify_roles')

const router = require('express').Router()

// PUBLIC ROUTES

// PRIVATE ROUTES
router.use(verifyToken)
// router.use(isModeratorOrAdmin)
router.get('/', user.getCurrent)

module.exports = router