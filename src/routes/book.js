const book = require('../controllers/book')
const express = require('express')
const verifyToken = require('../middlewares/verify_token')
const { isAdmin } = require('../middlewares/verify_roles') 

const router = express.Router()

// PUBLIC ROUTES
// router.get('/', getBooks)

// PRIVATE ROUTES
router.use(verifyToken)
router.use(isAdmin)
router.post('/', book.createNewBook)

module.exports = router