const db = require('../models')
const { Op } = require('sequelize')

// CREATE
const createNewBook = (body) => new Promise(async (resolve, reject) => {
   try {
      const response = await db.Book.findOrCreate({
         where: { title: body?.title },
         default: body
      })
      resolve({
         err: response[1] ? 0 : 1,
         mes: response[1] ? 'Created' : 'Cannot create new book',
      })
   } catch (error) {
      reject(error)
   }
})




// READ
// UPDATE
// DELETE

module.exports = {createNewBook}