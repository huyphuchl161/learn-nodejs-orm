const Joi = require("joi");

const email = Joi.string().pattern(new RegExp("gmail.com$")).required();

const password = Joi.string().min(6).required();

const title = Joi.string().required();

const price = Joi.number().required();

const available = Joi.number().required();

const category_code = Joi.string().uppercase().alphanum().required();

const image = Joi.string().required();

export { email, password, title, price, available, category_code, image };
